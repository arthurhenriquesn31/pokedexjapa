//
//  lista pokemonViewController.swift
//  pokedexjapa
//
//  Created by COTEMIG on 04/08/22.
//

import UIKit

class lista_pokemonViewController: UIViewController, UITableViewDataSource{
    @IBOutlet weak var tableView: UITableView!
    var pokemons: [Pokemon] = [
        Pokemon(nome: "Bulbassauro", tipo_1: "Grama", tipo_2: "Veneno", img: "bulbassa uro", color: "verde"),
        Pokemon(nome: "Ivyssauro", tipo_1: "Grama", tipo_2: "Veneno", img: "ivy2", color: "verde"),
        Pokemon(nome: "Venussauro", tipo_1: "Grama", tipo_2: "Veneno", img: "venussauro", color: "verde"),
        Pokemon(nome: "Charmander", tipo_1: "Fogo", tipo_2: nil, img: "charmander", color: "vermelho"),
        Pokemon(nome: "Charmeleon", tipo_1: "Fogo", tipo_2: nil, img: "charmeleon", color: "vermelho"),
        Pokemon(nome: "Charizard", tipo_1: "Fogo", tipo_2: "Voador", img: "charizard", color: "vermelho"),
        Pokemon(nome: "Squirtle", tipo_1: "Grama", tipo_2: nil, img: "squirtle", color: "azul"),
        Pokemon(nome: "Wartotle", tipo_1: "Grama", tipo_2: "Veneno", img: "wartotle", color: "azul"),
        Pokemon(nome: "Blastoise", tipo_1: "Grama", tipo_2: "Veneno", img: "blastoise", color: "azul"),
        Pokemon(nome: "Pikachu", tipo_1: "Grama", tipo_2: "Veneno", img: "pikachu", color: "amarelo")
    ]
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.dataSource = self
        // Do any additional setup after loading the view.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.pokemons.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as? TableViewCell else { return UITableViewCell() }
        let pokemon = pokemons[indexPath.row]
        cell.nome.text = pokemon.nome
        cell.tipo1.text = pokemon.tipo_1
        cell.tipo2.text = pokemon.tipo_2
        cell.imagem.image = UIImage(named: pokemon.img)
        cell.contentView.backgroundColor = UIColor(named: pokemon.color)
        return cell
    }

}
struct Pokemon {
    var nome: String
    var tipo_1: String?
    var tipo_2: String?
    var img: String
    var color: String

}
